

interface IMeta {
    title: string,
    description: string,
    robotsNoindex? : string,
    pathname: string,
    imagePath?: string,
    type?: 'website',
}