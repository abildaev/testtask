'use client';

import { useState, useEffect } from 'react';

function useIsClient() {
  const [isClient, setClient] = useState<boolean>(false);

  useEffect(() => {
    setClient(true);
  }, []);

  return isClient;
}

export default useIsClient;
