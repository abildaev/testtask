import clsx from 'clsx';
import Image from 'next/image';

import Check from '@assets/images/svgs/check.svg';
import Cross from '@assets/images/svgs/cross.svg';
import Loader from '@assets/images/svgs/loader.svg';

const styles = {
  base: 'flex ml-auto items-center justify-center w-12 h-12 leading-none text-center whitespace-nowrap rounded-large select-none',
  theme: {
    'primary-green': 'bg-primary-green',
    'primary-pink': 'bg-primary-pink',
    'primary-blue': 'bg-primary-blue',
  },
  animation: 'animate-spin',
  content: {
    check: Check,
    cross: Cross,
    loading: Loader,
  },
};

interface InfoIconProps {
  className?: string;
  animation?: boolean;
  theme: 'primary-green' | 'primary-pink' | 'primary-blue';
  content: 'check' | 'cross' | 'loading';
}


const InfoIcon = ({ className, theme, content, animation }: InfoIconProps) => (
  <div className={clsx(styles.base, styles.theme[theme], className)}>
    <Image
      className={clsx(animation ? styles.animation : '')}
      src={styles.content[content]}
      width={48}
      height={48}
      alt=""
    />
  </div>
);

export default InfoIcon;
