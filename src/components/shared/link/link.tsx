import clsx from 'clsx';
import NextLink from 'next/link';
import React, { ReactNode } from 'react';

interface LinkProps {
  className?: string;
  href?: string;
  title?: string;
  size?: keyof typeof styles.size;
  theme?: keyof typeof styles.theme;
  children: ReactNode;
  to?: string
}

const styles = {
  transition: 'transition-colors duration-200',
  base: '',
  size: {
    md: 'font-base tracking-tight-8 font-normal hover:font-medium',
    sm: 'font-base tracking-tight-8 font-light hover:font-medium',
  },
  theme: {
    default: 'opacity-90',
  },
};

const Link = ({
                                     className: additionalClassName = '',
                                     href = '',
                                     size,
                                     theme,
                                     children,
                                     ...props
                                   }: LinkProps) => {


    const linkClassName = clsx(
      styles.transition,
      size && theme && styles.base,
      size && styles.size[size],
      theme && styles.theme[theme],
      additionalClassName
  );

  if (href.startsWith('/')) {
    return (
        <NextLink href={href} {...props} className={linkClassName}>
          {children}
        </NextLink>
    );
  }

  return (
      <a className={linkClassName} href={href} {...props}>
        {children}
      </a>
  );
};

export default Link;
