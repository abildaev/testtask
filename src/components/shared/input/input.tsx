import React, {forwardRef, InputHTMLAttributes} from 'react';
import clsx from 'clsx';

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
    className?: string;
}

const Input = forwardRef<HTMLInputElement, InputProps>(({className, placeholder, type, ...otherProps}, ref) => (
    <input
        className={clsx('outline-none', className)}
        ref={ref}
        placeholder={placeholder}
        type={type}
        {...otherProps}
    />
));

export default Input;