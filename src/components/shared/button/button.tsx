import React from 'react';
import clsx from 'clsx';

import Link from '@components/shared/link';

// Определение типов для размера и темы кнопки
type ButtonSize = 'md' | 'sm';
type ButtonTheme = 'primary-blue-outline' | 'primary-blue-filled';

// Определение стилей кнопки
const styles = {
    base: 'flex items-center rounded-large leading-4 text-base font-semibold px-9 md:px-4 whitespace-nowrap duration-200 transition-colors',
    size: {
        md: 'h-12',
        sm: 'h-11',
    },
    theme: {
        'primary-blue-outline':
            'bg-primary-blue/20 border border-primary-blue tracking-tight-8 hover:bg-primary-blue/50',
        'primary-blue-filled': 'bg-primary-blue border border-transparent hover:bg-secondary-blue',
    },
};

interface ButtonProps {
    className?: string;
    to?: string;
    size: ButtonSize;
    theme: ButtonTheme;
    children: React.ReactNode;
    type?: "submit" | "button" | "reset"
}

const Button = ({ className, to, size, theme, children, ...otherProps }: ButtonProps )=> {
    if (to) {
        return (
            <Link
                className={clsx(styles.base, styles.size[size], styles.theme[theme], className )}
                to={to}
                {...otherProps}
            >
                {children}
            </Link>
        );
    } else {
        return (
            <button
                className={clsx(styles.base, styles.size[size], styles.theme[theme], className )}
                {...otherProps}
            >
                {children}
            </button>
        );
    }
};

export default Button;
