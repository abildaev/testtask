import Header from '@components/shared/header';
import localFont from 'next/font/local'

import '@styles/global.css'
import {ReactNode} from "react";



const localInter = localFont({
    variable: '--font-inter',
    src: [
        {
            path: "../assets/fonts/inter/inter-light.woff2",
            weight: "300",
            style: "normal",

        },
        {
            path: "../assets/fonts/inter/inter-regular.woff2",
            weight: "400",
            style: "normal",
        },
        {
            path: "../assets/fonts/inter/inter-medium.woff2",
            weight: "500",
            style: "normal",
        }
    ],
});


const localAeonik = localFont({
    variable: '--font-aeonik',
    src: [
        {
            path: "../assets/fonts/aeonik/aeonik-regular.woff2",
            weight: "400",
            style: "normal",
        }
    ],
});


const RootLayout = ({ children }: {children: ReactNode}) => (
  <html lang="en">
    <body className={`${localInter.variable} ${localAeonik.variable} font-sans `}>
      <Header />
      <main>{children}</main>
    </body>
  </html>
);

export default RootLayout;
