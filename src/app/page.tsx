import Hero from '@components/pages/home/hero';
import getMetadata from '@utils/get-metadata';
import { Metadata } from 'next'


export async function generateMetadata(): Promise<Metadata> {
  return getMetadata({
    title: 'Home - DevOptima',
    description: 'DevOptima home page',
    pathname: '/',
  });
}

const Home = () => (
    <Hero />
);

export default Home;


